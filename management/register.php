<?php
require_once "inc/config.php";

$req = $db->prepare("SELECT * FROM `user` WHERE email=? AND password=?");
$req->execute(array( $_REQUEST['Email'], $_REQUEST['Password']));
$data = $req->fetch();

if ($data) {
	$_SESSION['userid'] = $data['id'];
} else {
	unset($_SESSION['userid']);
}

header('location:index.php');
?>