<!doctype html>
<?php require_once 'inc/config.php';?>
<html class="no-js" lang="en">
<?php require_once 'templates/head.php';?>
<body>
	<?php require_once 'templates/header.php';?>
	<div class="row">
		<div class="columns small-12 medium-12 large-12 large-centered">
			<form class="login-form" method="post" action="register.php"">

				<div class="login">
					<span class="edit-form-label">Email</span>
					<span><input name="Email" class="login-email-input"></input></span>
				</div>

				<div class="login">
					<span class="edit-form-label">Mot de passe</span>
					<span><input name="Password" class="login-pswd-input"></input></span>
				</div>

				<div class="edit-form-send"><input class="edit-form-send-button" type="submit" value="OK" /></div>
			</form>
		</div>
	</div>

	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
</body>