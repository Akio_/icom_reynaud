<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>TE1 RESPONSIVE</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width; initial-scale=1">
	<link rel="stylesheet" href="main.css">


	<style>
		body{
			.container{
				overflow: hidden;
				width: 680px;
				height: auto;
				background: #EEEEEE;
				margin: 0px auto;
				padding-top: 20px;
			}
			.news{
				margin-left: 20px;
				float: left;
				width: 145px;
				height: 145px;
				background: #ff4d4d;
				margin-bottom: 20px;
			}
			.subarticle{
				float: left;
				width: 310px;
				height: 200px;
				background: #66cc66;
				margin-left: 20px;
			}

			@media only screen and (max-width: 640px){
				.container{
					width: 340px;
					height: 740px;
				}
				.news{
					margin-bottom: 10px;
					margin-left: 5px;
					margin-right: 5px;
				}
				.complement{
					margin-left: 15px;
				}
				.subarticle{
					float: left;
					height: 200px;
					width: 300px;
					background: #66cc66;
					margin-bottom: 10px;
					margin-left: 20px;
					margin-bottom: 10px;
				}
			}
		</style>
	</head>


	<body>
		<div class="container">
			<header class="mainheader">
			</header>
			<section class="mainsection">
				<aside class="complement">
					<article class="news">
					</article>
					<article class="news">
					</article>
					<article class="news">
					</article>
					<article class="news">
					</article>
				</aside>
				<main class="body">
					<article class="headline">
					</article>

					<article class="subarticle">
					</article>
					<article class="subarticle">
					</article>
					
				</main>

			</section>

		</div>
	</body>
	</html>